class Like < ApplicationRecord
  belongs_to :likeable, polymorphic: true
  belongs_to :user

  validate :like_limit


  def like_limit
    user_that_give_the_like = User.find_by(id: user_id)
    likes_of_user_that_give_the_like = user_that_give_the_like.likes
    likes_that_have_the_same_likeable_likes_of_user_that_give_the_like = likes_of_user_that_give_the_like.where(likeable_id: likeable_id).where(likeable_class: likeable_class)
    if (not (likes_that_have_the_same_likeable_likes_of_user_that_give_the_like.empty?))
      errors.add(:user,"Já deu o like nesse negocio") 
    end
  end
end
