class User < ApplicationRecord

    has_many :posts
    has_many :likes
    has_many :comments

    validates :name, :email, :birth,  presence: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } , uniqueness: true
    validate :max_age
    validates :name, length: {minimum:2, maximum:100}

    has_many :friendship_ones, :class_name => 'FriendShip', :foreign_key => :friend_id
    has_many :friend_ones, through: :friendship_ones
    has_many :friendship_twos, :class_name => 'FriendShip', :foreign_key => :user_id
    has_many :friend_twos, through: :friendship_twos

    def friends
        User.joins('friend_ships').where("(user_id = (?) or friend_id = (?)) and accept = true",id,id)
    end


    def self.names
        all
    end 





    private
        def max_age
            return if birth.nil?
            errors.add(:age, "muito velho para entra no facebook") if birth < (Date.today - 150.year)
        end

end
