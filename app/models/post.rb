class Post < ApplicationRecord
  belongs_to :user
  has_many :comments
  has_many :likes, as: :likeable

  validates :message, presence: true,length: {minimum:1, maximum:200}
end
