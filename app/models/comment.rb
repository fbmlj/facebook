class Comment < ApplicationRecord
    belongs_to :user
    belongs_to :post
    has_many :likes, as: :likeable

    validates :message, presence: true, length: {minimum:1, maximum:200}
end
